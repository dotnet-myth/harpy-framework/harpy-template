﻿using Exemple.Domain.AggregateModels;
using Myth.Interfaces.Repositories.EntityFramework;

namespace Exemple.Domain.Interfaces.Repositories {

    public interface IWeatherForecastRepository : IReadWriteRepositoryAsync<WeatherForecast> {
    }
}