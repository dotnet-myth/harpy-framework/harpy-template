﻿using Exemple.Domain.AggregateModels;
using Exemple.Domain.Commands;
using Exemple.Domain.Events;
using Exemple.Domain.Interfaces.Repositories;
using Harpy.Application.CommandHandlers;
using Harpy.Domain.Interfaces.Messages;
using System.Threading;
using System.Threading.Tasks;

namespace Exemple.Application.CommandHandlers {

    public class PostWeatherForecastCommandHandler : CommandHandler<PostWeatherForecastCommand, WeatherForecast> {
        private readonly IWeatherForecastRepository _weatherForecastRepository;

        public PostWeatherForecastCommandHandler(
            IMessageBus bus,
            IWeatherForecastRepository weatherForecastRepository )
            : base( bus ) {
            _weatherForecastRepository = weatherForecastRepository;
        }

        public override async Task<WeatherForecast> Handle( PostWeatherForecastCommand command, CancellationToken cancellationToken ) {
            var weatherForecast = new WeatherForecast(
                command.Date,
                command.TemperatureC,
                command.Summary );

            await _weatherForecastRepository.AddAsync( weatherForecast, cancellationToken );

            await _weatherForecastRepository.SaveChangesAsync( cancellationToken );

            var @event = new WeatherForecastCreatedEvent( weatherForecast.WeatherForecastId );

            await _bus.RaiseEventAsync( @event, cancellationToken );

            return weatherForecast;
        }
    }
}