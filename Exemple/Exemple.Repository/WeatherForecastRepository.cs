﻿using Exemple.Context;
using Exemple.Domain.AggregateModels;
using Exemple.Domain.Interfaces.Repositories;
using Myth.Repositories.EntityFramework;

namespace Exemple.Repository {

    public class WeatherForecastRepository : ReadWriteRepositoryAsync<WeatherForecast>, IWeatherForecastRepository {

        public WeatherForecastRepository( ExempleContext context ) : base( context ) {
        }
    }
}