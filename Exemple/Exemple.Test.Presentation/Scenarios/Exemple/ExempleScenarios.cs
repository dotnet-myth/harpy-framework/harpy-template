﻿using Harpy.Test.Presentation;
using Presentation.Api;
using Xunit.Abstractions;

namespace Exemple.Test.Presentation.Scenarios.Exemple {

    public class ExempleScenarios : ScenarioBase<Startup> {

        public ExempleScenarios( TestFixture<Startup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }
    }
}