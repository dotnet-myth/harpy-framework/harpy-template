﻿using Exemple.Presentation.ViewModels;
using Exemple.Test.Presentation.Scenarios.Exemple.WeatherForecast.Base;
using Harpy.Test.Presentation;
using Microsoft.AspNetCore.Http.Extensions;
using Myth.Repositories.Results;
using Presentation.Api;
using System.Linq;
using System.Net;
using Xunit;
using Xunit.Abstractions;

namespace Exemple.Test.Presentation.Scenarios.Exemple.WeatherForecast {

    public class GetWeatherForecastScenarios : WeatherForecastScenarios {

        public GetWeatherForecastScenarios( TestFixture<Startup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }

        [Fact]
        public async void Get_all_weather_forecast_ok( ) {
            // Arrange
            var queryBuilder = new QueryBuilder {
                { "$pageSize", "5" },
                { "$orderBy", "WeatherForecastId" }
            };

            // Act
            var response = await _client
                .DoGet( _baseUrl + "WeatherForecast" + queryBuilder.ToString( ) )
                .When( config => config
                    .StatusIs<Paginated<WeatherForecastViewModel>>( HttpStatusCode.OK ) )
                .BuildResultAsync( );

            // Assert
            Assert.NotNull( response );
            Assert.Equal( HttpStatusCode.OK, response.StatusCode );
            Assert.Equal( typeof( Paginated<WeatherForecastViewModel> ), response.ResultType );

            var result = response.GetAs<Paginated<WeatherForecastViewModel>>( );
            Assert.True( result.Itens.Any( ) );
            Assert.Equal( 5, result.Itens.Count( ) );
        }

        [Fact]
        public async void Get_first_weather_forecast_ok( ) {
            // Act
            var response = await _client
                .DoGet( _baseUrl + "WeatherForecast/1" )
                .When( config => config
                    .StatusIs<WeatherForecastViewModel>( HttpStatusCode.OK ) )
                .BuildResultAsync( );

            // Assert
            Assert.NotNull( response );
            Assert.Equal( HttpStatusCode.OK, response.StatusCode );
            Assert.Equal( typeof( WeatherForecastViewModel ), response.ResultType );

            var result = response.GetAs<WeatherForecastViewModel>( );
            Assert.NotNull( result );
        }
    }
}