﻿using Exemple.Presentation.ViewModels;
using Exemple.Test.Presentation.Scenarios.Exemple.WeatherForecast.Base;
using Harpy.Test.Presentation;
using Presentation.Api;
using System;
using System.Net;
using Xunit;
using Xunit.Abstractions;

namespace Exemple.Test.Presentation.Scenarios.Exemple.WeatherForecast {

    public class PostWeatherForecastScenarios : WeatherForecastScenarios {

        public PostWeatherForecastScenarios( TestFixture<Startup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }

        [Fact]
        public async void Post_weather_forecast_ok( ) {
            // Arrange
            var date = DateTime.Now;
            var temperature = 25;
            var summary = "Hot";
            var request = new PostWeatherForecastViewModel( date, temperature, summary );

            // Act
            var response = await _client
                .DoPost( _baseUrl + "WeatherForecast", request )
                .When( config => config
                    .StatusIs<WeatherForecastViewModel>( HttpStatusCode.Created ) )
                .BuildResultAsync( );

            // Assert
            Assert.NotNull( response );
            Assert.Equal( HttpStatusCode.Created, response.StatusCode );
            Assert.Equal( typeof( WeatherForecastViewModel ), response.ResultType );

            var actual = response.GetAs<WeatherForecastViewModel>( );
            Assert.NotNull( actual );
        }
    }
}