﻿using Harpy.Test.Presentation;
using Presentation.Api;
using Xunit.Abstractions;

namespace Exemple.Test.Presentation.Scenarios.Exemple.WeatherForecast.Base {

    public class WeatherForecastScenarios : ExempleScenarios {
        protected string _baseUrl { get; set; } = "api/Exemple/";

        public WeatherForecastScenarios( TestFixture<Startup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }
    }
}