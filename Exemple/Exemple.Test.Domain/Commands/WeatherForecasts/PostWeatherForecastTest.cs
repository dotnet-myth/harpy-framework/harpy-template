﻿using Exemple.Application.CommandHandlers;
using Exemple.Context;
using Exemple.Domain.AggregateModels;
using Exemple.Domain.Commands;
using Exemple.Domain.Interfaces.Repositories;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Interfaces.Validations;
using Harpy.Test.Domain.Commands;
using Harpy.Test.Domain.Mock.Messages;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Exemple.Test.Domain.Commands.WeatherForecasts {

    public class PostWeatherForecastTest : CommandTest {
        protected readonly IWeatherForecastRepository _weatherForecastRepository;

        protected readonly IValidation<PostWeatherForecastCommand> _validator;

        protected readonly IMessageBus _bus;

        public PostWeatherForecastTest(
                ExempleContext context,
                IWeatherForecastRepository weatherForecastRepository )
                : base( context ) {
            _weatherForecastRepository = weatherForecastRepository;
            _bus = MemoryBus.Mock( );
        }

        private async Task Mock( ) {
            await CleanContextAsync( );

            var hotForecast = new WeatherForecast( DateTime.Now, 25, "Hot" );
            await ( _context as ExempleContext ).Set<WeatherForecast>( ).AddAsync( hotForecast );

            await SaveContextAsync( );
        }

        [Fact]
        public async Task Weather_forecast_must_be_created( ) {
            await Mock( );

            Random random = new Random( );

            var command = new PostWeatherForecastCommand(
                DateTime.Now,
                10 + random.Next( ),
                "Freezing" );

            var handler = new PostWeatherForecastCommandHandler(
                _bus,
                _weatherForecastRepository );

            var result = await handler.Handle( command, CancellationToken.None );

            Assert.NotNull( result );
        }
    }
}