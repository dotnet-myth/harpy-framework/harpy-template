using Exemple.Context;
using Exemple.IoC;
using Harpy.IoC;
using Harpy.SQLite.IoC;
using Harpy.Test.Domain;
using Harpy.Test.Domain.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace Exemple.Test.Domain {

    public class Startup : StartupTestBase {

        public override void ConfigureServices( IServiceCollection services ) {
            if ( IsFirstCall( services ) ) {
                services.AddDatabase(
                    connection => connection.UseSqliteConnection( ),
                    context => {
                        context.AddContext<ExempleContext>( );
                    }
                );

                services.CreateTestDatabase<ExempleContext>( );

                services.AddExemple( triggerJobs: false );

                base.ConfigureServices( services );
            }
        }
    }
}