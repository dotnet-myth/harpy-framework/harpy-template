﻿using Harpy.IoC.Options;
using Microsoft.EntityFrameworkCore;
using Myth.Contexts;

namespace Exemple.Context {

    public class ExempleContext : BaseContext {

        public ExempleContext( DbContextOptions options, Parameters parameters = null ) : base( options ) {
        }
    }
}