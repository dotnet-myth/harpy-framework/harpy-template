﻿using Harpy.IoC.Constants;

namespace Exemple.Presentation {

    public class Feature : FeatureBase {

        public Feature( string name ) : base( name ) {
        }

        public static Feature PostTransactionExemple => new Feature( "PostTransaction" );

        public static Feature PostExemple => new Feature( "PostWithoutTransaction" );

        public static Feature GetAllExemples => new Feature( "GetAll" );

        public static Feature GetExemple => new Feature( "GetSingle" );
    }
}