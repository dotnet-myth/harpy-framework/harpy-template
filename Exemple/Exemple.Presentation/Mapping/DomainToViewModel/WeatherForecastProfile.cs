﻿using AutoMapper;
using Exemple.Domain.AggregateModels;
using Exemple.Presentation.ViewModels;

namespace Exemple.Presentation.Mapping.DomainToViewModel {

    public class WeatherForecastProfile : Profile {

        public WeatherForecastProfile( ) {
            CreateMap<WeatherForecast, WeatherForecastViewModel>( );
        }
    }
}