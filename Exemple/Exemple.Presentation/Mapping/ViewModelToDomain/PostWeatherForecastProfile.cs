﻿using AutoMapper;
using Exemple.Domain.Commands;
using Exemple.Presentation.ViewModels;

namespace Exemple.Presentation.Mapping.ViewModelToDomain {

    public class PostWeatherForecastProfile : Profile {

        public PostWeatherForecastProfile( ) {
            CreateMap<PostWeatherForecastViewModel, PostWeatherForecastCommand>( );
        }
    }
}