﻿using Exemple.Domain.AggregateModels;
using Exemple.Domain.Commands;
using Exemple.Domain.Interfaces.Queries;
using Exemple.Domain.Jobs;
using Exemple.Presentation.ViewModels;
using Harpy.IoC.Extensions;
using Harpy.Presentation.Controller;
using Harpy.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using Myth.Api;
using Myth.Interfaces.Repositories.Results;
using Myth.Repositories.Results;
using NSwag.Annotations;
using System.Threading;
using System.Threading.Tasks;

namespace Exemple.Presentation.Controllers {

    [Route( "api/Exemple/" )]
    [OpenApiTags( "Exemple / Weather Forecast" )]
    public class WeatherForecastController : ApiController {
        private readonly IWeatherForecastQuery _weatherForecastQuery;

        public WeatherForecastController(
                IWeatherForecastQuery weatherForecastQuery ) {
            _weatherForecastQuery = weatherForecastQuery;
        }

        [FeatureGate( nameof( Feature.GetAllExemples ) )]
        [HttpGet( "[controller]" )]
        [OpenApiOperation( "Get all weather forecast",
                "Return all forecasts save in database. \n" +
                "At this endpoint, operations similar to the ODATA standard can be used. \n" +
                "Filter example:\n" +
                "- temperatureC gt 10 ( temperature in celsius greater than 10 )\n" +
                "- temperatureC le -10 ( celsius temperature less than or equal to -10 )\n" +
                "- summary eq \"Hot\" ( Summary equals \"hot\" )\n" +
                "\n" +
                "Sorting examples:     \n" +
                "- summary             \n" +
                "- temperatureC desc   \n" +
                "- temperatureF asc    \n " +
                "\n" +
                "Paging examples: \n" +
                "- PageNumber: 2 \n" +
                "- PageSize: 2" )]
        [ProducesResponseType( typeof( Paginated<WeatherForecastViewModel> ), StatusCodes.Status200OK )]
        public async Task<IActionResult> GetAsync( Odata<WeatherForecastViewModel, WeatherForecast> odata, CancellationToken cancellationToken ) {
            var weatherForecasts = await _weatherForecastQuery.GetAsync( odata, cancellationToken );
            var result = weatherForecasts.MapTo<IPaginated<WeatherForecastViewModel>>( );

            return Ok( result );
        }

        [FeatureGate( nameof( Feature.GetExemple ) )]
        [HttpGet( "[controller]/{weatherForecastId}" )]
        [OpenApiOperation( "Get single weather forecast", "Return forecast by id" )]
        [ProducesResponseType( typeof( WeatherForecastViewModel ), StatusCodes.Status200OK )]
        public async Task<IActionResult> GetAsync( [FromRoute] long weatherForecastId, CancellationToken cancellationToken ) {
            var weatherForecasts = await _weatherForecastQuery.GetAsync( weatherForecastId, cancellationToken );
            var result = weatherForecasts.MapTo<WeatherForecastViewModel>( );

            return Ok( result );
        }

        [FeatureGate( nameof( Feature.PostExemple ) )]
        [HttpPost( "[controller]" )]
        [OpenApiOperation( "Post weather forecast", "Create a new forecast" )]
        [ProducesResponseType( typeof( WeatherForecastViewModel ), StatusCodes.Status200OK )]
        public async Task<IActionResult> PostAsync( [FromBody] PostWeatherForecastViewModel postWeatherForecast, CancellationToken cancellationToken ) {
            var response = await CreateWeatherForecastAsync( postWeatherForecast, cancellationToken );

            RaiseExempleJobs( response.WeatherForecastId );

            return Created( "Get", response );
        }

        [FeatureGate( nameof( Feature.PostTransactionExemple ) )]
        [HttpPost( "[controller]/WithTransaction" )]
        [OpenApiOperation( "Post weather forecast using transaction",
            "Create a new forecast \n" +
            "## SQLite does not support multiple transactions, to use transactions do not use persistent jobs \n" +
            "## Uncomment the '.EnableTransaction( )' line at Startup" )]
        [ProducesResponseType( typeof( WeatherForecastViewModel ), StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<IActionResult> PostWithTransactionAsync( [FromBody] PostWeatherForecastViewModel postWeatherForecast, CancellationToken cancellationToken ) {
            using var transaction = new Transaction( );

            var response = await CreateWeatherForecastAsync( postWeatherForecast, cancellationToken );

            transaction.Commit( );

            RaiseExempleJobs( response.WeatherForecastId );

            return Created( "Get", response );
        }

        private static async Task<WeatherForecastViewModel> CreateWeatherForecastAsync( PostWeatherForecastViewModel postWeatherForecast, CancellationToken cancellationToken ) {
            // Create a new weather forecast by command
            var command = postWeatherForecast.MapTo<PostWeatherForecastCommand>( );

            var result = await command
                .SendAsync( cancellationToken )
                .MapToAsync<WeatherForecast, WeatherForecastViewModel>( );

            return result;
        }

        private static void RaiseExempleJobs( long weatherForecastId ) {
            // Raise a simple job
            var simpleJob = new WeatherForecastUpdatedJob( weatherForecastId );
            var jobId = simpleJob.Raise( );

            // Raise a continued job, after the end of simple job
            new WeatherForecastContinuedJob( weatherForecastId, jobId ).Raise( );
        }
    }
}