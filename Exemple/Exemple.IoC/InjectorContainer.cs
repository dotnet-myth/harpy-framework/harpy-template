﻿using Exemple.Domain.Jobs;
using Harpy.IoC;
using Microsoft.Extensions.DependencyInjection;

namespace Exemple.IoC {

    public static class InjectorContainer {

        private static IServiceCollection AddUpdateJob( this IServiceCollection services ) {
            var updateJob = new WeatherForecastPersistedJob( );
            services.RaiseInBackground( updateJob );

            return services;
        }

        public static IServiceCollection AddExemple( this IServiceCollection services, bool triggerJobs = true ) {
            services.AddRepositories( );
            services.AddQueries( );

            if ( triggerJobs )
                services.AddUpdateJob( );

            return services;
        }
    }
}