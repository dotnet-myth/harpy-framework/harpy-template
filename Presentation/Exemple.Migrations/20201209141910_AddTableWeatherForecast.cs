﻿using FluentMigrator;

namespace Exemple.Migrations {

    [Migration( 20201209141910 )]
    public class _20201209141910_AddTableWeatherForecast : Migration {

        public override void Up( ) {
            Create
                .Table( "weather_forecast" )
                .WithColumn( "weather_forecast_id" ).AsInt64( ).PrimaryKey( )
                .WithColumn( "date" ).AsDateTime( ).NotNullable( )
                .WithColumn( "temperature_c" ).AsInt32( ).NotNullable( )
                .WithColumn( "temperature_f" ).AsInt32( ).Nullable( )
                .WithColumn( "summary" ).AsString( );
        }

        public override void Down( ) {
            Delete.Table( "weather_forecast" );
        }
    }
}