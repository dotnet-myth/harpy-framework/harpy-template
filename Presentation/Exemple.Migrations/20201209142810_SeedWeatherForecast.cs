﻿using FluentMigrator;
using System;

namespace Exemple.Migrations {

    [Migration( 20201209142810 )]
    public class _20201209142810_SeedWeatherForecast : Migration {

        public override void Up( ) {
            var rng = new Random( );
            var summaries = new[ ] { "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching" };

            for ( var i = 0; i < 10; i++ )
                Insert
                    .IntoTable( "weather_forecast" )
                    .Row( new { date = DateTime.Now.AddDays( -i ), temperature_c = rng.Next( -20, 55 ), summary = summaries[ rng.Next( summaries.Length ) ] } );
        }

        public override void Down( ) {
            Delete
                .FromTable( "weather_forecast" )
                .AllRows( );
        }
    }
}