using Exemple.Context;
using Exemple.IoC;
using Hangfire;
using Harpy.IoC;
using Harpy.Presentation.Extensions;
using Harpy.SQLite.IoC;
using Harpy.SQLite.Presentation.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Myth.Extensions;
using System.IO;

namespace Presentation.Api {

    public class Startup {
        private readonly string _defaultConnetion;

        private readonly string _hangfireConnection;

        private readonly IConfiguration _configuration;

        public Startup( IConfiguration configuration ) {
            _configuration = configuration;
            _defaultConnetion = _configuration.GetConnectionStringFile( "DefaultConnection" );
            _hangfireConnection = _configuration.GetConnectionStringFile( "HangfireConnection" );
        }

        public void ConfigureServices( IServiceCollection services ) {
            services.AddSwaggerMiddleware( AppVersion.GetCurrent( ) );

            services.AddDefaultCorsPolice( );

            services.AddCulture( "en-US" );

            services.AddDatabase(
                connection => connection.UseSqliteConnection( _defaultConnetion, options =>
                    options.AddMigrations( "Exemple.Migrations" )
                ),
                context => {
                    context.AddContext<ExempleContext>( );
                },
                options => options
                    .EnableLog( )
                    .EnableMigrations( migrationOptions =>
                       migrationOptions.EnsureAllMigrationsApplied( ) )
            );

            services.AddHarpy( settings => settings.JobStorage = HangfireSQLite.UseStorage( _hangfireConnection ) );
            services.AddExemple( );

            services.AddHealthCheck( _hangfireConnection );

            services.AddEndpoints( );
        }

        public void Configure( IApplicationBuilder app ) {
            app.UseStaticFiles( file => file
                .IncludePath( Directory.GetCurrentDirectory( ), "images" )
            );

            app.UseCors( "DefaultPolicy" );

            app.UseHarpySwagger( );

            app.UseHangfireDashboard( );

            app.UseExceptionMiddleware( );

            app.UseAuthorizationMiddleware( );

            app.UseRouting( );

            app.UseAuthentication( );

            app.UseAuthorization( );

            app.UseEndpoints( endpoints => endpoints.MapControllers( ) );

            app.UseHttpsRedirection( );
        }
    }
}